[33mcommit 8e70ac9ddbaa4b5aa5242c33272dac2fa1f6e397[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 20:48:46 2021 +0300

    assignment-image-rotation

[33mcommit fc8b6420380c011706d4f734d140fa16b2f5b805[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 20:45:24 2021 +0300

    assignment-image-rotation

[33mcommit a3173ae8b5cf87c7f83908495d9518fea9d90177[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 20:34:23 2021 +0300

    assignment-image-rotation

[33mcommit 710593dfd7ae723cc135efb1aa245e24f56b37ac[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 20:28:19 2021 +0300

    assignment-image-rotation

[33mcommit a8afb3fa4420fad18783b19a4e40e485024153b4[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 20:17:12 2021 +0300

    assignment-image-rotation

[33mcommit 28101bc7b92b8ade944d584575de374570f38e0b[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 19:53:12 2021 +0300

    src

[33mcommit 01fde9d9c81cbe8ad8a8aa08e2434aa4c3f430a6[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 19:07:38 2021 +0300

    update

[33mcommit 7eb9b09f8340a3e548ddc09b14cad648e89371b1[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 18:37:23 2021 +0300

    .

[33mcommit a710be2cd440584e655e2ea7c854295a0739cd21[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 18:30:11 2021 +0300

    .

[33mcommit bc933a99a0ffa1fd9e6026a74355d8a3dd6a6e98[m
Author: emnhaque <291184@niuitmo.ru>
Date:   Mon Dec 27 02:03:14 2021 +0300

    .

[33mcommit a3021ad091006dd838bad61118c8c77b1561984b[m
Merge: f1ff6f8 7ce37b9
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Tue Sep 7 18:32:40 2021 +0000

    Merge branch 'feature/build-system' into 'master'
    
    Update build processes
    
    See merge request programming-languages/assignment-image-rotation!6

[33mcommit 7ce37b994984a960560c1e25a5f00b4d24ffb4d1[m
Author: Nikita Akatyev <frfntdakatevin@gmail.com>
Date:   Fri Apr 30 19:16:58 2021 +0300

    Update build processes

[33mcommit f1ff6f8317aa6bfa0c9116be6535cc259e283c0f[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Thu Apr 29 15:17:43 2021 +0200

    fix test running message

[33mcommit 1d757dd70bddce8d93c6bde4c0f8bbe8425b661b[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Thu Apr 29 15:17:27 2021 +0200

    stub

[33mcommit 99f3b2531f08da2d3701a2dfa0944753a4029860[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Thu Apr 29 15:05:29 2021 +0200

    update

[33mcommit e3a275f5129e300225e2c765d84084aac8b8bd68[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Thu Apr 29 15:00:50 2021 +0200

    update

[33mcommit 0399167a127057617f8c28632d89ef74493445bf[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Fri Apr 16 13:52:12 2021 +0200

    extract header viewer into a separate repository

[33mcommit be2e87871474a1c2999ce24525ca11b7604d6530[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Fri Mar 19 20:52:27 2021 +0100

    update

[33mcommit 608854e5261312bb0706cf16a2828e89c8b7475d[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Sun Feb 7 21:59:24 2021 +0100

    Fixes

[33mcommit 3911189e9973a44172a56ab9171dc9b34f921046[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Tue Jan 26 03:09:09 2021 +0300

    Update common-mistakes.md

[33mcommit 87daaf659124160736bda4517c935fab3f00a939[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Tue Jan 26 03:05:31 2021 +0300

    Add new file

[33mcommit 4de941015ca7d0a2ffc6d25e6bd541581ccae57e[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Sun Jan 24 00:31:30 2021 +0300

    Update README.md

[33mcommit a713f2efdf5c64a71db3807adbf86affde9b1a3d[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Fri Jan 8 22:31:29 2021 +0300

    Update README.md

[33mcommit 9f97aa3eab2625485999bdf63779f77624600583[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Fri Jan 8 19:59:35 2021 +0300

    Update README.md

[33mcommit 9f19713ca65fb76d21790a285f760979f8910d62[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Wed Jan 6 00:11:46 2021 +0100

    Update instructions

[33mcommit bf2a55a542432f132743a5ef6975fc8bdd11e161[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Wed Jan 6 00:03:45 2021 +0100

    Modify README

[33mcommit 0e5f34a92ed0ce5b478b87653582b1df170f5c16[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Wed Jan 6 02:01:04 2021 +0300

    Update README.md

[33mcommit 409b47efbd4548a4c31104fa07b775e80f5699ce[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Wed Dec 30 22:57:25 2020 +0100

    Update the assignment task text

[33mcommit 82c0522a860d6178c4503ea345837897bb7eb927[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Sat Dec 26 19:37:41 2020 +0100

    Update the description

[33mcommit 6c5e42dd6d48d69a3721efadceb7f3043d99255b[m
Author: Igor Zhirkov <igorjirkov@gmail.com>
Date:   Sat Dec 26 19:34:38 2020 +0100

    Add a simple tool to view bmp header files

[33mcommit aaa4465aec29e005938ae28b6a215c2efc8ae28b[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Wed Dec 23 04:24:03 2020 +0300

    Update README.md

[33mcommit 603167c0691bb99a443fd027530d227fb0af96e2[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Sat Dec 19 03:04:56 2020 +0300

    Update README.md

[33mcommit 61360a0cf33c3f449e37627cb4be7e096780249f[m
Author: Igor Zhirkov <igorjirkov@itmo.ru>
Date:   Wed Oct 21 16:29:19 2020 +0300

    Initial commit
