//
// Created by emnhaque on 23.12.2021.
//
#include "../include/bmp.h"
#include "../include/file_io.h"

#include <stdio.h>


#define BISIZE 40
#define BIBITCOUNT 24
#define BFTYPE 0x4D42
#define ZERO 0

long long  bmp_get_padding(struct image const *img){
    return (long long)(img->width%4);
}

struct bmp_header create_bmp_header(struct image const *img){
    struct  bmp_header header = {
            .bfType = BFTYPE,
            .bfileSize = img->width * img->height * sizeof(struct pixel) + img->height * bmp_get_padding(img) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),

            .biSize = BISIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = BIBITCOUNT,
            .biCompression = ZERO,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
    header.biSizeImage = header.bfileSize - header.bOffBits;
    return header;
}

enum read_status check_header(const struct bmp_header header){
    if(header.bfType != BFTYPE ) return READ_INVALID_SIGNATURE;
    if(header.biBitCount != BIBITCOUNT){return READ_INVALID_BITS;}
    if(header.biSize != BISIZE
       || header.biCompression != ZERO
       || header.bfileSize != header.bOffBits + header.biSizeImage) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

// check  read file from BMP to struct image and return status
enum read_status from_bmp( FILE * path, struct image* img ){

    if(path == NULL){return READ_INVALID_PATH;}
    struct bmp_header header;

    size_t res;
    res = fread(&header,1,sizeof(struct bmp_header),path);
    if(res != sizeof (struct bmp_header)) return READ_INVALID_HEADER;

    const enum read_status check_header_status = check_header(header);
    if(check_header_status) {
        return check_header_status;
    }

    *img = creation_image(header.biWidth,header.biHeight);
    if(img->data == NULL) {return READ_INVALID_SIGNATURE;}

    long long padding = bmp_get_padding(img);
    //doc file theo tung dong cua header
    for (size_t i = 0; i <  header.biHeight ; i++){
        fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, path);
        fseek(path, padding, SEEK_CUR);
    }
    if(img->data == NULL) {return READ_ERROR;}
        /*
        size_t res_h = fread(&(img->data[i * img->width]),sizeof(struct pixel), img->width, path);
        //used to move file pointer associated with a given file to a specific position.
        //zero if successful, or else it returns a non-zero value
        if (img->width != res_h  || fseek(path,padding,SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }*/
    enum close_file_status status_close = file_close(path);
    if(status_close != CLOSE_OK)
    {
        return READ_ERROR;
    }
    return READ_OK;
}

// check  write file from struct image to BMP image and return status
enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = create_bmp_header(img);
    long long padding = bmp_get_padding(img);

    // пишем заголовок
    size_t res = fwrite( &header, 1, sizeof(struct bmp_header), out );
    if( res != sizeof(struct bmp_header) ) {
        fclose(out);
        return WRITE_ERROR;
    }
    const struct pixel nulls[] = {{0},{0},{0}};
    for(size_t i = 0;i < img->height; i++){
        if(!fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) ||
           ( padding && !fwrite(nulls, 1, padding, out)))
        {return WRITE_ERROR;}
    }
    enum close_file_status closeFileStatus = file_close(out);
    if(closeFileStatus != CLOSE_OK){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static const char* read_status_string[] = {
        [READ_OK]                     = "Image form file is loaded\n",
        [READ_INVALID_SIGNATURE]      = "Invalid Signature. Check file format (Only 24-bit bpm file supported).\n",
        [READ_INVALID_BITS]           = "Only 24-bit bpm file supported\n",
        [READ_INVALID_HEADER]         = "Invalid file header\n",
        [READ_INVALID_PATH]           = "Input file path not found\n"
};
static const char*  write_status_string[] = {
        [WRITE_OK]      = "Image is saved in file\n",
        [WRITE_ERROR]   = "File write error\n"
};
enum read_status print_read_status(enum read_status status){
    printf("%s",read_status_string[status]);
    return status;
}
enum write_status print_write_status(enum write_status status){
    printf("%s",write_status_string[status]);
    return status;
}

