//
// Created by emnhaque on 23.12.2021.
//

#include "../include/file_io.h"

#include <stdio.h>


enum open_file_status check_open_read(FILE ** file,const char * filepath) {
    *file = fopen(filepath, "rb");
    if (!*file) {
        return OPEN_READ_ERROR;
    }
    return OPEN_OK;
}
enum open_file_status check_open_write(FILE ** file, const char * filepath){
    *file = fopen(filepath,"wb");
    if(!*file){
        return OPEN_WRITE_ERROR;
    }
    return OPEN_OK;
}

enum close_file_status file_close(FILE * file){
    if(!file){
        return CLOSE_ERROR;
    }
    fclose(file);
    return CLOSE_OK;
}


