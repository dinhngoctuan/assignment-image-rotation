//
// Created by emnhaque on 23.12.2021.
//

/* создаёт копию изображения, которая повёрнута на 90 градусов */

#include "image.h"
#include "rotation.h"
struct pixel calculate_pixel(struct image source, uint64_t col, uint64_t row){
    return source.data[source.width * row + col];
}
struct image rotate( struct image const source){

    struct image img_rotation = creation_image(source.height,source.width);

    for (uint32_t row = 0; row < source.height; row++) {
        for (uint32_t col = 0; col < source.width; col++) {

            //img_rotation.data[(col*source.height) + (source.height - 1 - row)] = source.data[col + (row*source.width)];
            img_rotation.data[(col*source.height) + (source.height - 1 - row)] = calculate_pixel(source,col,row);
        }
    }

    return img_rotation;
}
struct image rotate_90_clockwise(struct image const img){
    uint64_t a = img.height;
    uint64_t b = img.width;
    struct image img_rotation = creation_image(a,b);
    for (uint64_t i = 0; i < a; i++) {
        for (uint64_t j = 0; j < b; j++) {
            //img_rotation.data[i+((img_rotation.height-j-1) * img_rotation.width)] = img.data[j+i * b];
            img_rotation.data[i+((img_rotation.height-j-1) * img_rotation.width)] = calculate_pixel(img,i,j);
        }
    }
    return img_rotation;
}
struct image transformation(struct image source,enum status_rotated status){
    if(status == ROTATE90_DEGREES){
        return rotate(source);
    }
    if (status == ROTATE90_CLOCKWISE){
        return rotate_90_clockwise(source);
    }
    else {
        return source;
    }
}


