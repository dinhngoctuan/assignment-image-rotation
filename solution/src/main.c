#include "../include/bmp.h"
#include "../include/file_io.h"
#include "../include/rotation.h"

#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3) {
        return 1;
    }
    const char *input_path = argv[1];
    const char *output_path = argv[2];


    FILE * input = NULL;
    struct image img = {0};
    enum open_file_status status_or = check_open_read(&input,input_path);
    if(status_or != OPEN_OK) { return status_or;}
    enum read_status status_r = from_bmp(input,&img);
    if(status_r != READ_OK){
        print_read_status(status_r);
    }

    struct image rotated_img = transformation(img,ROTATE90_DEGREES);
    free_bmp(&img);

    FILE *output = NULL;
    enum open_file_status status_ow = check_open_write(&output,output_path);
    if(status_ow != OPEN_OK ){
        return status_ow;
    }
    uint8_t status_w = to_bmp(output,&rotated_img);
    if(status_w != WRITE_OK){
        print_write_status(status_w);
    }

    free_bmp(&rotated_img);
    return 0;
}
