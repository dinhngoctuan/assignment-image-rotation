//
// Created by emnhaque on 25.12.2021.
//
#include "../include/image.h"

#include <malloc.h>
#include <stdint.h>



// function create image
struct image creation_image(uint64_t width, uint64_t height) {
    struct image img = {
            .width = width,
            .height = height,
            .data = malloc(width*height*sizeof(struct pixel))
    };
    return img;
}

void free_bmp(struct image *img){
    free(img->data);
}
