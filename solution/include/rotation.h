//
// Created by emnhaque on 23.12.2021.
//
#pragma once

#ifndef ASSIGNMENT_IMAGE_ROTATION1_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION1_ROTATION_H

#include "image.h"
#include "malloc.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image source);
enum status_rotated{
    ROTATE90_DEGREES,
    ROTATE90_CLOCKWISE,

};
struct image transformation(struct image source,enum status_rotated status);
struct pixel calculate_pixel(struct image source, uint64_t col, uint64_t row);
#endif //ASSIGNMENT_IMAGE_ROTATION1_ROTATION_H
