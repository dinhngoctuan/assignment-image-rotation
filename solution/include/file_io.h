//
// Created by emnhaque on 23.12.2021.
//
#pragma once

#ifndef ASSIGNMENT_IMAGE_ROTATION1_FILE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION1_FILE_IO_H

#include <stdio.h>

enum open_file_status {
    OPEN_OK,
    OPEN_WRITE_ERROR ,
    OPEN_READ_ERROR ,
};

enum close_file_status{
    CLOSE_OK = 0,
    CLOSE_ERROR,
};
enum open_file_status check_open_read(FILE ** file,const char * filepath);
enum open_file_status check_open_write(FILE ** file, const char * filepath);
enum close_file_status file_close(FILE * file);

#endif //ASSIGNMENT_IMAGE_ROTATION1_FILE_IO_H

