//
// Created by emnhaque on 23.12.2021.
//
#pragma once

#include "../include/image.h"

#include  <stdint.h>
#include <stdio.h>


#ifndef ASSIGNMENT_IMAGE_ROTATION1_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION1_BMP_H

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;  // 0x4d42 | 0x4349 | 0x5450
    uint32_t  bfileSize;//размер файла
    uint32_t bfReserved; // 0
    uint32_t bOffBits; // смещение до поля данных,
                        // обычно 54 = 16 + biSize
    uint32_t biSize; // размер струкуры в байтах:
                        // 40(BITMAPINFOHEADER) или 108(BITMAPV4HEADER)
                        // или 124(BITMAPV5HEADER)
    uint32_t biWidth; // ширина в точках
    uint32_t  biHeight;// высота в точках
    uint16_t  biPlanes; // всегда должно быть 1
    uint16_t biBitCount; // 0 | 1 | 4 | 8 | 16 | 24 | 32
    uint32_t biCompression; // BI_RGB | BI_RLE8 | BI_RLE4 |
                            // BI_BITFIELDS | BI_JPEG | BI_PNG
                            // реально используется лишь BI_RGB
    uint32_t biSizeImage; // Количество байт в поле данных
                            // Обычно устанавливается в 0
    uint32_t biXPelsPerMeter; // горизонтальное разрешение, точек на дюйм
    uint32_t biYPelsPerMeter;// вертикальное разрешение, точек на дюйм
    uint32_t biClrUsed; // Количество используемых цветов
                        // (если есть таблица цветов)
    uint32_t  biClrImportant; // Количество существенных цветов.
                              // Можно считать, просто 0
};
#pragma pack(pop)
/*  deserializer */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PATH,
    READ_ERROR,
    /* коды других ошибок  */
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};
struct bmp_header create_bmp_header(struct image const *img);

enum read_status from_bmp( FILE * path, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );

enum read_status print_read_status(enum read_status status);
enum write_status print_write_status(enum write_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION1_BMP_H
