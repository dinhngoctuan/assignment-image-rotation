//
// Created by emnhaque on 25.12.2021.
//
#pragma once


#ifndef ASSIGNMENT_IMAGE_ROTATION1_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION1_IMAGE_H

#include <stdint.h>
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

struct image creation_image(uint64_t width, uint64_t height);
void free_bmp(struct image *img);
#endif //ASSIGNMENT_IMAGE_ROTATION1_IMAGE_H
